package cl.kevin.test.awto.kevintestawto.services.impl;

import cl.kevin.test.awto.kevintestawto.entities.HashTag;
import cl.kevin.test.awto.kevintestawto.entities.HashTagLog;
import cl.kevin.test.awto.kevintestawto.entities.Log;
import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;
import cl.kevin.test.awto.kevintestawto.repositories.HashTagLogRepository;
import cl.kevin.test.awto.kevintestawto.repositories.HashTagRepository;
import cl.kevin.test.awto.kevintestawto.repositories.LogRepository;
import cl.kevin.test.awto.kevintestawto.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class LogServiceImpl implements LogService {
  @Autowired
  private LogRepository logRepository;
  @Autowired
  private HashTagRepository hashTagRepository;
  @Autowired
  private HashTagLogRepository hashTagLogRepository;

  @Override
  @Transactional
  public Log create (LogRequest log) {
   Set<HashTag> hashTagsRelated = new HashSet<>();
   Set<String> hashTags = log.getHashTags();
   Set<HashTagLog> hashTagLogs = new HashSet<>();
   HashTag newHashTag;
   Log newLog = new Log();
   newLog.setHost(log.getHost());
   newLog.setDetails(log.getDetails());
   newLog = logRepository.save(newLog);
   for (String hashTag: hashTags) {
     HashTagLog newHashTagLog =  new HashTagLog();
     if (!this.hashTagRepository.findByDescription(hashTag).isPresent()) {
       newHashTag = new HashTag();
       newHashTag.setDescription(hashTag.replaceAll("#", ""));
       newHashTag = this.hashTagRepository.save(newHashTag);
     } else {
       newHashTag = this.hashTagRepository.findByDescription(hashTag).get();
     }
     newHashTagLog.setHashTag(newHashTag);
     newHashTagLog.setLog(newLog);
     hashTagsRelated.add(newHashTag);
     hashTagLogs.add(newHashTagLog);
   }
   hashTagRepository.saveAll(hashTagsRelated);
   hashTagLogRepository.saveAll(hashTagLogs);
   newLog.setHashTags(hashTagLogs);
   return newLog;
  }
}
