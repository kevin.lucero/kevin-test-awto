package cl.kevin.test.awto.kevintestawto.repositories;

import cl.kevin.test.awto.kevintestawto.entities.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<Log, Integer> {
}
