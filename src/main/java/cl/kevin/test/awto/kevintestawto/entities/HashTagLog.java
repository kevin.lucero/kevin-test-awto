package cl.kevin.test.awto.kevintestawto.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "aw_hashtag_log")
public class HashTagLog {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "log_id")
  private Log log;

  @ManyToOne
  @JoinColumn(name = "hashtag_id")
  private HashTag hashTag;
}
