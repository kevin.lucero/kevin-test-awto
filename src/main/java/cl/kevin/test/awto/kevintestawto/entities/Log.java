package cl.kevin.test.awto.kevintestawto.entities;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString

@AllArgsConstructor
@NoArgsConstructor
@Table(name = "aw_log")
public class Log {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @CreationTimestamp
  @Column(name = "creation_date", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  @NonNull
  @NotEmpty
  @Column(name = "host")
  private String host;

  @Column(name = "details")
  private String details;

  @JsonManagedReference
  @OneToMany(mappedBy = "hashTag")
  private Set<HashTagLog> hashTags = new HashSet<>();
}
