package cl.kevin.test.awto.kevintestawto.services;

import cl.kevin.test.awto.kevintestawto.entities.Log;
import cl.kevin.test.awto.kevintestawto.json.request.LogRequest;

public interface LogService {
  Log create(LogRequest log);
}
